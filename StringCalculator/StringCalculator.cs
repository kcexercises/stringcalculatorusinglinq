﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var stringNumbersArray = GetNumbersArrayFromDelimiteredString(numbers);
            var numbersArray = Array.ConvertAll(stringNumbersArray, int.Parse);
            
            return Sum(numbersArray);
        }

        private string[] GetNumbersArrayFromDelimiteredString(string numbers)
        {
            if (numbers.StartsWith("//"))
            {
                return GetNumbersArrayWithCustomDelimiter(numbers);
            }

            return numbers.Split(new char[] {',','\n'});
        }

        private string[] GetNumbersArrayWithCustomDelimiter(string numbers)
        {
            var numbersAndDelimiters = numbers.Split("\n");
            numbersAndDelimiters[0] = numbersAndDelimiters[0].Replace("//", "");

            if (numbersAndDelimiters[0].StartsWith("[") && numbersAndDelimiters[0].EndsWith("]"))
            {
                var delimiters = numbersAndDelimiters[0].Split(new char[] {'[', ']'}, 
                    StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine(delimiters);
                return numbersAndDelimiters[1].Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }

             Console.WriteLine(numbersAndDelimiters[0]);
            return numbersAndDelimiters[1].Split(numbersAndDelimiters[0], StringSplitOptions.RemoveEmptyEntries);
        }

        private int Sum(int[] numbersArray)
        {
            var negativeNumbers = new List<int>();
            var numbersList = new List<int>(); 

            foreach(var number in numbersArray.Where(value => value < 1001))
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number);
                }

                numbersList.Add(number);
            }
                
            CheckForNegativeNumbers(negativeNumbers);

            return numbersList.Sum();
        }

        private void CheckForNegativeNumbers(List<int> negativeNumbers)
        {
            if (negativeNumbers.Any())
            {
                throw new Exception("Negatives not allowed: "+ string.Join(",", negativeNumbers));
            }
        }
    }
}
